# Aplicando Conceitos de Coleções Arrays e Listas
## Ferramentas Utilizadas
* IntelliJ
## Ementa
### O que são Collections
* [x] - Apresentação do curso, objetivos e requisitos mínimos
* [x] - Introdução ao tema
* [x] - Criando o projeto
* [x] - Introdução e desenvolvimento de arrays
* [x] - Como funciona o intArrayOf
* [x] - Prática com Array de strings
* [x] - Prática com DoubleArray
* [x] - Operações com Arrays max, min, average e filter
* [x] - Operações com Arrays count, find e any
* [x] - Refatorando operações maxOrNull e minOrNull
* [x] - Desenvolvimento com listOf
* [x] - Organizando a lista com sortedBy
* [x] - Organizando grupos com groupBy
* [x] - Organizando conjuntos com setOf
* [x] - Desenvolvimento com mapOf

### O que são collections mutáveis

* [x] - Conceitos iniciais
* [] - Desenvolvimento com mutableListOf
* [] - Desenvolvimento com mutableMapOf - Parte 1
* [] - Desenvolvimento com mutableMapOf - Parte 2
* [] - Introdução a Extensions Functions
* [] - Prática com Extensions Functions
* [] - Conclusão do curso
* [x] - Certifique seu conhecimento

## 🏆 Certifique seu Conhecimento
### 1 - É Correto afirmar sobre collections : 
* [x] - A api de collections provê collections mutáveis e não mutáveis
### 2 - Em qual(is) alternativa(s) conseguimos criar um Map corretamente : 
``` kotlin
// Resposta : 
  mapOf("chave" to "valor")
``` 
### 3 - Sobre funções estendidas podemos afirmar : 
* [x] - Possibilita estender uma classe adicionando novas funcionalidades
### 4 - É Correto afirmar sobre collections mutáveis : 
* [x] - Kotlin além das collections não mutáveis, oferece as coleções mutáveis e nessas podemos alterá-las utilizando comportamentos como adicionar e remover elementos
### 5 - Sobre as operações union, intersect e subtract com Set
* [x] - Utilizando union de um conjunto com um segundo conjunto, obtemos um novo conjunto com o merge dos dois conjuntos eliminando elementos repetidos, se houver
### 6 - Sobre sortBy e sortedBy podemos afirmar:
* [x] - A função sortBy reorganiza os elementos da coleção que a invocou de acordo com um critério, enquanto a função sortedBy cria uma nova coleção ordenada de acordo com um critério, poupando a lista original
### 7 - Pensando em um Array de String, quais das maneiras (duas alternativas) abaixo é correto para inicializá-lo:
```kotlin
val contatos: Array<String> = Array<String>(3) { "" } contatos[0] = "João" contatos[1] = "Pedro" contatos[2] = "Maria"
```
### 8 - Dado a classe Funcionário, entenda-se que exista uma lista de Funcionários, qual seria a alternativa correta para agrupar os funcionários por setor:
```kotlin
data class Funcionario(
   val nome: String,
   val setor: String
)
```
```kotlin
// Resposta : 
funcionarios.groupBy { it.setor }
```
### 9 - Dado o Map abaixo, como conseguimos resgatar o salário do João:
```kotlin
     val pair = Pair("Joao", 1000.0)
        val map = mapOf(pair)
``` 
```kotlin
// Resposta : 
val salario = map["Joao"]
```
### 10 - Dado o array abaixo, qual das operações retornaria um array com os resultados abaixo :
```kotlin
val idades = intArrayOf(10, 20, 21, 22, 30, 31, 40, 43, 50)
Esperado: [40, 43, 50]
```
```kotlin
// Resposta : 
val result = idades.filter { it > 31 }
```
