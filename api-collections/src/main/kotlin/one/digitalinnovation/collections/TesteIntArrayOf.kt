package one.digitalinnovation.collections

fun main() {
    val values = intArrayOf(2, 3, 4, 1, 10, 7)
    println("Iteração 1")
    values.forEach {
        println(it)
    }

    println()

    println("Iteração 2")
    values.sort()
    values.forEach {
        println(it)
    }


}