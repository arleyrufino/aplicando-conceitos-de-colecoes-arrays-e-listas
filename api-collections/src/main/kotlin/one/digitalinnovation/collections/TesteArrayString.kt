package one.digitalinnovation.collections

fun main() {
    // O Array de String recebe o tamanho e o valor da inicialização
    val nomes = Array(3){""}
    nomes[0] = "Maria"
    nomes[1] = "João"
    nomes[2] = "José"

    println()
    println("Variável Nome 1")
    for(nome in nomes) {
        println(nome)
    }
    nomes.sort()
    nomes.forEach { println(it) }

    println()
    println("Variável Nome 2")
    val nome2 = arrayOf("Maria","Zazá","Pedro")
    nome2.sort()
    nome2.forEach { println(it) }
}